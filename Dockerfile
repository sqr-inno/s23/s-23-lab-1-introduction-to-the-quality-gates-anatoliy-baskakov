FROM eclipse-temurin:17-alpine

WORKDIR /app
COPY target/main-0.0.1-SNAPSHOT.jar /app/main.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "main.jar"]
