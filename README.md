# Lab 1 -- Introduction to the quality gates

[![pipeline status](https://gitlab.com/sqr-inno/s23/s-23-lab-1-introduction-to-the-quality-gates-anatoliy-baskakov/badges/main/pipeline.svg)](https://gitlab.com/sqr-inno/s23/s-23-lab-1-introduction-to-the-quality-gates-anatoliy-baskakov/-/commits/main)

## Homework

The project gets deployed to Fly. Check out [here](https://sqr-lab-1.fly.dev/hello).

